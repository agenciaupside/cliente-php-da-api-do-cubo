# Cliente PHP da API do Cubo

## Como usar?
```php
<?php
use AgenciaUpside\CuboApiClient\CuboApiClient;

$client = new CuboApiClient([
    'personal_access_token' => '...'
]);
```

## Exemplo 1 - Listando clientes
```php
<?php
...

$response = $client->clients->list();
foreach($response->body as $cliente) {
    echo "$cliente->brand\n";
}
```
