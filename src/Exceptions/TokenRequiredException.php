<?php
namespace AgenciaUpside\CuboApiClient\Exceptions;

use Throwable;

class TokenRequiredException extends \Exception {
    public function __construct(Throwable $previous = null) {
        parent::__construct(
            "Você precisa informar o Personal Access Token",
            1,
            $previous
        );
    }
}
