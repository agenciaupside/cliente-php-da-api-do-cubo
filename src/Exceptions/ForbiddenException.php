<?php
namespace AgenciaUpside\CuboApiClient\Exceptions;

use Throwable;

class ForbiddenException extends \Exception {
    public function __construct(Throwable $previous = null) {
        parent::__construct(
            "Proibido",
            403,
            $previous
        );
    }
}
