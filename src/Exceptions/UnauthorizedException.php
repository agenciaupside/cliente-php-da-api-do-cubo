<?php
namespace AgenciaUpside\CuboApiClient\Exceptions;

use Throwable;

class UnauthorizedException extends \Exception {
    public function __construct(Throwable $previous = null) {
        parent::__construct(
            "Não autorizado",
            401,
            $previous
        );
    }
}
