<?php
namespace AgenciaUpside\CuboApiClient;

use AgenciaUpside\CuboApiClient\Exceptions\TokenRequiredException;
use Httpful\Request;

class CuboApiClient {
    public $clients, $contacts, $headers, $options, $last_error;

    public function __construct($options = []) {
        $this->options = array_merge([
            'api_endpoint'      => 'https://cubo2.agenciaupside.com/api',
            'api_version'       => 'v1',
            'throws_exception'  => true
        ], $options);

        //default headers
        $this->headers = [
            'Accept' => 'application/json',
        ];

        if (isset($this->options['personal_access_token']) && !is_null($this->options['personal_access_token'])) {
            $this->headers['Authorization'] = "Bearer {$this->options['personal_access_token']}";
        } else {
            throw new TokenRequiredException();
        }

        $this->clients = new Clients($this);
        $this->contacts = new Contacts($this);
    }

    protected function setLastError($last_error) {
        $this->last_error = $last_error;
    }

    public function getLastError() {
        return $this->last_error;
    }

    public function delete($url, array $data = [], array $query_parameters = [], array $headers = [])
    {
        return $this->request('delete', $url, $data, $query_parameters, $headers);
    }

    public function get($url, array $data = [], array $query_parameters = [], array $headers = [])
    {
        return $this->request('get', $url, $data, $query_parameters, $headers);
    }

    public function patch($url, array $data = [], array $query_parameters = [], array $headers = [])
    {
        return $this->request('patch', $url, $data, $query_parameters, $headers);
    }

    public function post($url, array $data = [], array $query_parameters = [], array $headers = [])
    {
        return $this->request('post', $url, $data, $query_parameters, $headers);
    }

    public function put($url, array $data = [], array $query_parameters = [], array $headers = [])
    {
        return $this->request('put', $url, $data, $query_parameters, $headers);
    }

    public function request($method = 'get', $url, array $data = [], array $query_parameters = [], array $headers = [])
    {
        $request = Request::$method($url.(!empty($query_parameters) ? "?".http_build_query($query_parameters) : ""), 'application/json')
            ->addHeaders(array_merge($this->headers, $headers))
            ->sendsJson();

        if (!empty($data))
            $request->body(json_encode($data));

        $response = $request
            //->autoParse(false)
            ->send();
        return $response;
    }
}
