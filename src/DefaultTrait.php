<?php
namespace AgenciaUpside\CuboApiClient;

use AgenciaUpside\CuboApiClient\Exceptions\ForbiddenException;
use AgenciaUpside\CuboApiClient\Exceptions\UnauthorizedException;

trait DefaultTrait {
    /** @var CuboApiClient $client */
    public $client, $resource;

    public function __construct(&$client) {
        $this->client = $client;
        $this->resource = strtolower(substr(strrchr(__CLASS__, "\\"), 1));
    }

    public function create($data, $query_parameters = [], $headers = []) {
        return $this->processes_response(
            $this->client->post("{$this->client->options['api_endpoint']}/{$this->client->options['api_version']}/{$this->resource}", $data, $query_parameters, $headers),
            201
        );
    }

    public function edit($id, $data, $query_parameters = [], $headers = []) {
        return $this->processes_response($this->client->patch("{$this->client->options['api_endpoint']}/{$this->client->options['api_version']}/{$this->resource}/$id", $data, $query_parameters, $headers));
    }

    public function delete($id, $query_parameters = [], $headers = []) {
        return $this->processes_response($this->client->delete("{$this->client->options['api_endpoint']}/{$this->client->options['api_version']}/{$this->resource}/$id", [], $query_parameters, $headers));
    }

    public function list($query_parameters = [], $headers = []) {
        return $this->processes_response($this->client->get("{$this->client->options['api_endpoint']}/{$this->client->options['api_version']}/{$this->resource}", [], $query_parameters, $headers));
    }

    public function show($id, $query_parameters = [], $headers = []) {
        return $this->processes_response($this->client->get("{$this->client->options['api_endpoint']}/{$this->client->options['api_version']}/{$this->resource}/$id", [], $query_parameters, $headers));
    }

    private function processes_response($response, $expected_code = 200) {
        if ($response->code != $expected_code && $this->client->options['throws_exception']) {
            switch ($response->code) {
                case 401:
                    throw new UnauthorizedException();
                    break;
                case 403:
                    throw new ForbiddenException();
                    break;
                default:
                    throw new \Exception($response->body->message);
            }
        }

        return $response;
    }
}
